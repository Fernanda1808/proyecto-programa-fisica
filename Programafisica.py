import pygame, sys 
from pygame.locals import * 
import math 

pygame.init()
DISPLAYGAME=pygame.display.set_mode ((500,400), 0, 32)
pygame.display.set_caption("Dibujando figuras") 

negro=(0,0,0)
blanco=(255,255,255)
rojo=(255,0,0)
verde=(0,255,0)
azul=(0,0,255)
myclock=pygame.time.Clock() 
DISPLAYGAME.fill(blanco) 

radio=10
alfa=30
beta=30
alfa=alfa*math.pi/180 #Formula para pasar de grados a radianes
beta=beta*math.pi/180 
m1=50
m2=50

def hipotenusa(alfa,beta):
    c=math.pi-alfa-beta 
    return 300*math.sin(beta)/math.sin(c) 

h=hipotenusa(alfa,beta)
c1=(h+radio)/2 
c2=(h+radio)/2

print(h)
x=100+h*math.cos(alfa) #Coordenadas del vertice de arriba 
y=350-h*math.sin(alfa)


s1=math.sqrt(pow(c1,2)+ pow(radio,2)) 
s2=math.sqrt(pow(c2,2)+ pow(radio,2))
 
while True: 
    for event in pygame.event.get():
        if event.type==QUIT: 
            pygame.quit()
            sys.exit()
   
    DISPLAYGAME.fill(blanco)
    pygame.draw.polygon(DISPLAYGAME, azul, [[100,350], [400,350], [x, y]], 0) 
    c1=[x-math.cos(alfa)*(s1+radio), y+math.sin(alfa)*(s1-radio)]
    c2=[x+math.cos(beta)*(s2+radio), y+math.sin(beta)*(s2-radio)] #se calculan los centros de los circulos 
    pygame.draw.line(DISPLAYGAME,verde,[x,y], c1)
    pygame.draw.line(DISPLAYGAME,verde,[x,y], c2) 
    pygame.draw.circle(DISPLAYGAME, rojo,c1, radio )
    pygame.draw.circle(DISPLAYGAME, rojo,c2, radio ) 
    pygame.display.update()
    if (m1>m2):                #Indica cual masa es mas pesada para que la distancia aumente y la otra disminuya 
        s1+=1 if s1<h else 0 
        s2-=1 if s2>0 else 0 
    elif (m1<m2): 
        s2+=1 if s2<h else 0 
        s1-=1 if s1>0 else 0 
    myclock.tick(50)   
    